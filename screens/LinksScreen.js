import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text, Image } from 'react-native';
import axios from 'axios'


const Item = ({ data, name, image, description }) => (
  <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 30}}>
    <Image source={{ uri: image}} style={{ height: 100, width: 100}} />
    <View style={{ flex: 1, paddingLeft: 15 }}>
    <Text>
      Name: {name}
    </Text>
    <Text>
      Description: {description}
    </Text>
    <Text>
      Barcode: {data}
    </Text>
    </View>
  </View>
)
export default class ListBarcodeScreen extends Component {

  state = {
    data: [],
    loading: false
  }
  componentDidMount() {
    this.getData()
  }

  getData = () => {
    this.setState({loading: true})
    axios.get('https://barcode.code4change.tech/api/barcodes',{},
      {
        data: {},
        params: {},
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((response) => {
        this.setState({ data: response.data, loading: false })
      })
      .catch((error) => {
        this.setState({loading: false})
      })
      .finally(function () {
        // always executed
      });
  }
  render() {
    return (
      <View style={styles.container}>
        {/**
         * Go ahead and delete ExpoLinksView and replace it with your content;
         * we just wanted to provide you with some helpful links.
         */}
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => <Item {...item} />}
          keyExtractor={item => item.id}
          onRefresh={this.getData}
          refreshing={this.state.loading}
        />
      </View>
    );
  }
}

ListBarcodeScreen.navigationOptions = {
  title: 'Barcode',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    backgroundColor: '#fff',
  },
});
