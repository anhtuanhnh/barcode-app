import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView, Modal, ActivityIndicator, TextInput } from 'react-native';
import { Camera } from 'expo-camera';
import { remove } from 'ramda'
import axios from 'axios'
import * as firebase from 'firebase';
import uuid from 'uuid';

export default class App extends Component {
  state = {
    hasPermission: null,
    scanCompleted: false,
    previewing: false,
    taking: false,
    images: [],
    modalVisible: false,
    itemBarcode: null,
    loading: false,
    description: '',
    name: '',
  }

  async componentDidMount() {
    const { status } = await Camera.requestPermissionsAsync();
    this.setHasPermission(status === 'granted');
  }

  handleBarCodeScanned = ({ type, data }) => {
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  setHasPermission = (status) => {
    this.setState({
      hasPermission: status,
    })
  }

  onScanAgain = () => {
    this.setState({ scanCompleted: false, previewing: false })
    if (this.camera) {
      this.camera.resumePreview()
    }
  }

  onBarCodeScanned = (data) => {
    const { scanCompleted } = this.state
    if (!scanCompleted) {
      if (this.camera) {
        this.camera.pausePreview()
        setTimeout(() => {
          this.handleBarCodeScanned(data)
          this.setState({ scanCompleted: true, data, previewing: true })
        }, 200)
      }
    }
  }


  onSnap = async () => {
    this.setState({ taking: true, previewing: false })
    if (this.camera) {
      let photo = await this.camera.takePictureAsync({
        // base64: true,
        quality: 0
      });

      this.setState({
        images: [].concat(this.state.images, {
          ...photo,
          content: this.state.data
        })
      })
      this.camera.resumePreview()
      this.setState({ taking: false })
    }

  }

  onToggleModal = (item, index) => {
    if (item) {
      this.setState({ itemBarcode: { ...item, index }, modalVisible: !this.state.modalVisible })
    } else {
      this.setState({ modalVisible: !this.state.modalVisible })
    }
  }

  onDeleteBarcode = (index) => {
    const newData = remove(index, 1, this.state.images)
    this.setState({ images: newData, modalVisible: false, itemBarcode: null })
  }

  onUploadBarcode = async (itemBarcode) => {
    this.setState({ loading: true, modalVisible: false, })
    if (itemBarcode) {
      const uploadImage = await this.uploadImageAsync(itemBarcode.uri)

      axios.post('https://barcode.code4change.tech/api/barcodes', {
        image: uploadImage,
        data: itemBarcode.content.data,
        type: itemBarcode.content.type,
        description: this.state.description,
        name: this.state.name,
        createAt: new Date()
      })
        .then((response) => {
          alert('Upload success')
          this.setState({ loading: false })
        })
        .catch(function (error) {
          alert('Upload fail')
        });
    }

  }

  uploadImageAsync = async (uri) => {
    // Why are we using XMLHttpRequest? See:
    // https://github.com/expo/expo/issues/2402#issuecomment-443726662
    const blob = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function (e) {
        reject(new TypeError('Network request failed'));
      };
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);
      xhr.send(null);
    });

    const ref = firebase
      .storage()
      .ref()
      .child(uuid.v4());
    const snapshot = await ref.put(blob);

    // We're done with the blob, close and release it
    blob.close();

    return await snapshot.ref.getDownloadURL();
  }
  render() {
    const { hasPermission, scanCompleted, previewing, images, taking, modalVisible, itemBarcode, loading, description, name } = this.state;

    if (hasPermission === null) {
      return <View />;
    }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    }

    return (
      <View style={{ flex: 1 }}>
        <Camera
          ref={ref => {
            this.camera = ref;
          }}
          style={{ flex: 1 }}
          onBarCodeScanned={this.onBarCodeScanned}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            {
              scanCompleted && !previewing && !taking && <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0, alignItems: 'center' }}>
                <TouchableOpacity style={{ padding: 10, backgroundColor: 'white' }} onPress={this.onScanAgain}>
                  <Text style={{ color: 'black' }}>
                    CONTINUE
            </Text>
                </TouchableOpacity>
              </View>
            }
            {
              previewing && <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0, alignItems: 'center', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity style={{ padding: 10, backgroundColor: 'white' }} onPress={this.onSnap}>
                  <Text style={{ color: 'black' }}>
                    TAKE A PICTURE
            </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginLeft: 10, padding: 10, backgroundColor: 'white' }} onPress={this.onScanAgain}>
                  <Text style={{ color: 'black' }}>
                    SCAN AGAIN
            </Text>
                </TouchableOpacity>
              </View>
            }
            {
              taking && <View style={{ position: 'absolute', bottom: 20, left: 0, right: 0, alignItems: 'center' }}>
                <TouchableOpacity style={{ padding: 10, backgroundColor: 'white' }}>
                  <Text style={{ color: 'black' }}>
                    TAKING A PICTURE
            </Text>
                </TouchableOpacity>
              </View>
            }
            {/* <View style={{ position: 'absolute', left: 15, bottom: 20, height: 30, width: 50, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
              <Text>
                {images.length}
              </Text>
            </View> */}
          </View>
        </Camera>
        <View style={{ height: 150, paddingVertical: 5, width: '100%', }}>

          <ScrollView
            contentContainerStyle={{}}
            horizontal={true}
          >
            {
              images.map((image, index) => (
                <View key={index} style={{ paddingHorizontal: 15, alignItems: 'center', justifyContent: 'center' }}>
                  <TouchableOpacity
                    onPress={() => this.onToggleModal(image, index)}
                    style={{ height: 100, width: 100 * (image.width / image.height), backgroundColor: 'gray' }}>
                    <Image source={{ uri: image.uri }} style={{ height: 100, width: 100 * (image.width / image.height) }} />
                  </TouchableOpacity>
                  {
                    image.content && <>
                      <Text>{image.content.type}</Text>
                      <Text>{image.content.data}</Text>
                    </>
                  }
                </View>
              ))
            }

          </ScrollView>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={modalVisible}
        >
          <View style={{ padding: 20, flex: 1, alignItems: 'center' }}>
            {
              itemBarcode &&
              <View>
                <Text style={{ paddingVertical: 15 }}>{itemBarcode.data}</Text>
                <Image source={{ uri: itemBarcode.uri }} style={{ height: 300, width: 300 * (itemBarcode.width / itemBarcode.height) }} />
              </View>
            }
            <View style={{width: '100%', marginTop: 20 }}>
            <Text>Name</Text>
              <TextInput style={{ width: '100%', padding: 5, borderColor: 'gray', borderWidth: 1 }} value={name} onChangeText={(name) => { this.setState({name})}} />
            <Text style={{ marginTop: 10}}>Description</Text>
              <TextInput style={{ width: '100%', padding: 5, borderColor: 'gray', borderWidth: 1, marginTop: 10 }} value={description} onChangeText={(description) => { this.setState({description})}} />
            </View>
            <View style={{ flexDirection: 'row', width: '100%', padding: 30, justifyContent: 'space-between' }}>
              <TouchableOpacity style={{ padding: 10, backgroundColor: 'gray' }} onPress={() => { this.onUploadBarcode(itemBarcode) }}>
                <Text style={{ color: 'black' }}>
                  UPLOAD
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 10, backgroundColor: 'gray' }} onPress={() => { this.onDeleteBarcode(itemBarcode.index) }}>
                <Text style={{ color: 'black' }}>
                  DELETE
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 10, backgroundColor: 'gray' }} onPress={this.onToggleModal}>
                <Text style={{ color: 'black' }}>
                  CANCEL
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        {
          loading && <View style={{ position: 'absolute', zIndex: 100, top: 0, left: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0.0.7)' }}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        }
      </View>
    );
  }
}